All functional components contain a runner file to run the component through a java application class.

All aspects of the components has being propertised, and can be controlled through the resources/properties file.

------------------------AWSS3UploadAndRDSConnections package----------------------
contains a components to connect to a mySQL database and execute select, update and Delete. (to control what database to connect to, and what to select, update and delete,
you need to input the sql queries you need in the properties file, there are sample queries there that is only meant to be a sample and will not connect to any actual database.)

also contains a component to connect to AWS s3 storage and can upload a selection of files.(to control what S3 storage to connect, you need to input sample parameters in the properties file.)


------------------------EWS package----------------------
a component to send emails via the exchange server. the setup can be configured from the resource/properties file, including setting up a proxy.

------------------------RESTAPI package----------------------
a sample rest api call to an test api interface.