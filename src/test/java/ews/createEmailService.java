package ews;

import java.net.URI;
import java.net.URISyntaxException;

import utilities.EWSEmailConfigFileReader;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.WebProxy;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;

public class createEmailService {
	public static ExchangeService createAEmailService() throws URISyntaxException, Exception {
		try {
			ExchangeService service = new ExchangeService(ExchangeVersion.valueOf(EWSEmailConfigFileReader.getEmailExchangeVersionFromPropFile()));
			if(!EWSEmailConfigFileReader.getEmailSenderFromPropFile().equalsIgnoreCase("")&&
					!EWSEmailConfigFileReader.getToEmailReceiverFromPropFile().isEmpty()) {
				ExchangeCredentials ExCredentials = new WebCredentials(EWSEmailConfigFileReader.getUserFromPropFile(), EWSEmailConfigFileReader.getPassFromPropFile());
				service.setCredentials(ExCredentials);

				if(EWSEmailConfigFileReader.getHasProxyFromPropFile().equalsIgnoreCase("true") &&
						!EWSEmailConfigFileReader.getProxyHostFromPropFile().equalsIgnoreCase("")) {
					WebProxy proxy = new WebProxy(EWSEmailConfigFileReader.getProxyHostFromPropFile(), EWSEmailConfigFileReader.getProxyPortFromPropFile());
					service.setWebProxy(proxy);
				}
				//not using auto discover as it takes up to 10 mins to find the url, assuming that
				//the uri is not going to change, the uri is set manually
				service.autodiscoverUrl(EWSEmailConfigFileReader.getEmailSenderFromPropFile(),new RedirectionUrlCallback());
				//service.setUrl(new URI(EWSEmailConfigFileReader.getEmailURIFromPropFile()));
				return service;
			}
			service.close();
			throw new Exception();
		}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		
		}
	}
