package ews;

import utilities.EWSEmailConfigFileReader;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.property.complex.MessageBody;

public class SendMail {



	public static boolean sendEmail(String subject,String messageBody) {
		try {
			ExchangeService service = createEmailService.createAEmailService();
			if(!EWSEmailConfigFileReader.getEmailSenderFromPropFile().equalsIgnoreCase("")&&
					!EWSEmailConfigFileReader.getToEmailReceiverFromPropFile().isEmpty()) {
				EmailMessage msg = new EmailMessage(service);
				System.out.println(messageBody);
				msg.setBody(MessageBody.getMessageBodyFromText(messageBody));
				
				msg.setSubject(subject);
				msg.getToRecipients().addEmailRange(EWSEmailConfigFileReader.getToEmailReceiverFromPropFile().iterator());
				if(EWSEmailConfigFileReader.getBCCEmailReceiverFromPropFile()!= null){msg.getBccRecipients().addEmailRange(EWSEmailConfigFileReader.getBCCEmailReceiverFromPropFile().iterator());}
				if(EWSEmailConfigFileReader.getCCEmailReceiverFromPropFile()!= null) {msg.getCcRecipients().addEmailRange(EWSEmailConfigFileReader.getCCEmailReceiverFromPropFile().iterator());}
				msg.getAttachments().addFileAttachment(EWSEmailConfigFileReader.getEmailAttachmentFromPropFile());
				msg.send();
				service.close();
				return true;
			}
			service.close();
			throw new Exception();

		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	

}
