package ews;

import microsoft.exchange.webservices.data.autodiscover.IAutodiscoverRedirectionUrl;

/**
 * this class implements the autodiscoverredirectionurl which will allow us to auto discover the URL of an exchange server
 * @author jiach
 *
 */
public class RedirectionUrlCallback implements IAutodiscoverRedirectionUrl {
    public boolean autodiscoverRedirectionUrlValidationCallback(
            String redirectionUrl) {
        return redirectionUrl.toLowerCase().startsWith("https://");
    }
}