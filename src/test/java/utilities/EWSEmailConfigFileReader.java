package utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import microsoft.exchange.webservices.data.property.complex.EmailAddress;

public class EWSEmailConfigFileReader {
	
	final static PropertiesFileReader propReader = new PropertiesFileReader();
	static Properties prop;
	final static String env = PropertiesFileReader.getChosenEnvironment().getProperty("ChosenEnvironment");
	

	
	public static String getUserFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"EmailCredentials.User")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"EmailCredentials.User");
			}
			else {
				throw new Exception("No User name is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No User name is entered in the properties file",e);
		}	
	}
	
	public static String getPassFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"EmailCredentials.Pass")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"EmailCredentials.Pass");
			}
			else {
				throw new Exception("No Password is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No Password is entered in the properties file",e);
		}	
	}
	
	public static String getHasProxyFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"hasProxy")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"hasProxy");
			}
			else {
				throw new Exception("No hasProxy is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No hasProxy is entered in the properties file",e);
		}	
	}
	
	public static String getProxyHostFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"proxyHost")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"proxyHost");
			}
			else {
				throw new Exception("No proxyHost is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No proxyHost is entered in the properties file",e);
		}	
	}
	
	public static int getProxyPortFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(prop.get(env+"ProxyPort") != null) {
				return Integer.parseInt((String) prop.get(env+"ProxyPort"));
			}
			else {
				throw new Exception("No ProxyPort is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No ProxyPort is entered in the properties file",e);
		}	
	}
	
	public static String getEmailAttachmentFromPropFile() throws Exception{
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"emailAttachment")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"emailAttachment");
			}
			else {
				throw new Exception("No emailAttachment is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No emailAttachment is entered in the properties file",e);
		}
	}
	
	public static String getEmailSenderFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"emailSender")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"emailSender");
			}
			else {
				throw new Exception("No emailSender is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No emailSender is entered in the properties file",e);
		}	
	}
	
	public static String getEmailExchangeVersionFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"exchangeVersion")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"exchangeVersion");
			}
			else {
				throw new Exception("No ExchangeVersion is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No ExchangeVersion is entered in the properties file",e);
		}	
	}
	
	public static List<EmailAddress> getToEmailReceiverFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"ToEmailReceiver")).equalsIgnoreCase("")) {
				String emailAddresses = prop.getProperty(env+"ToEmailReceiver");
				emailAddresses = emailAddresses.replace(",;", ";").replace(", ;", ";").replace(" ;", ";").replace(";;", ";");

				List<String> listOfEmailReceivers =  Arrays.asList(((emailAddresses)).split(";"));
				List<EmailAddress> listOfEmailAddress = new ArrayList<EmailAddress>();
				for(String email: listOfEmailReceivers) {
					listOfEmailAddress.add(new EmailAddress(email));
				}
				System.out.println(listOfEmailAddress);
				return listOfEmailAddress;
			}
			else {
				return null;
			}
		}catch(Exception e) {
			return null;		}	
	}
	
	public static List<EmailAddress> getCCEmailReceiverFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"CCEmailReceiver")).equalsIgnoreCase("")) {
				String emailAddresses = prop.getProperty(env+"CCEmailReceiver");
				emailAddresses = emailAddresses.replace(",;", ";").replace(", ;", ";").replace(" ;", ";");

				List<String> listOfEmailReceivers =  Arrays.asList(((emailAddresses)).split(";"));
				List<EmailAddress> listOfEmailAddress = new ArrayList<EmailAddress>();
				for(String email: listOfEmailReceivers) {
					listOfEmailAddress.add(new EmailAddress(email));
				}
				System.out.println(listOfEmailAddress);
				return listOfEmailAddress;
			}
			else {
				return null;
			}
		}catch(Exception e) {
			return null;		}	
	}
	
	public static List<EmailAddress> getBCCEmailReceiverFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"BCCEmailReceiver")).equalsIgnoreCase("")) {
				String emailAddresses = prop.getProperty(env+"BCCEmailReceiver");
				emailAddresses = emailAddresses.replace(",;", ";").replace(", ;", ";").replace(" ;", ";");
				List<String> listOfEmailReceivers =  Arrays.asList(((emailAddresses)).split(";"));
				List<EmailAddress> listOfEmailAddress = new ArrayList<EmailAddress>();
				for(String email: listOfEmailReceivers) {
					listOfEmailAddress.add(new EmailAddress(email));
				}
				System.out.println(listOfEmailAddress);
				return listOfEmailAddress;
			}
			else {
				return null;
			}
		}catch(Exception e) {
			return null;		}	
	}
	
	public static String getEmailURIFromPropFile() throws Exception {
		prop = getProperties();
		
		try {
			if(!(prop.getProperty(env+"emailURI")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"emailURI");
			}
			else {
				throw new Exception("No emailURI is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No emailURI is entered in the properties file",e);
		}	
	}
	
	private static Properties getProperties() throws IOException {
		Properties prop = PropertiesFileReader.getAgedCareEmailDetails();
		return prop;
	}
}
