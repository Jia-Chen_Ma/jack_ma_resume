
package utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

/**
 * This class will allow the user to create sets of data that he can use. Thus for example 
 * it will create sets of character strings that can be used to test ASCII data sets
 * it will create formatted date structures depending on the allowed format values e.g. 2/1/2019, 02/01/2019, 2 Jan 2019    
 * 
 *
 */
public class FormattedDataGenerator {

	private static Properties propertiesDateFormats;


	/**
	 * this method is used to change the format of the provided date with the integer passed as an argument
	 * The allowed set of date formats are in a properties file 
	 * also a value longer than the length of the properties file returns a random format.. 
	 * @param indate
	 * @param chosenDateFormatSelector
	 * @return returns a date in the chosen format - 0 means chose a random entry - 
	 */
	public static String getDateFormatted(Date indate, int chosenDateFormatSelector) {
		String chosenDateFormat = "yyyy-MM-dd HH-mm-ss";
		
		if (chosenDateFormatSelector > 0 && chosenDateFormatSelector < propertiesDateFormats.size() + 1) {

			chosenDateFormat = propertiesDateFormats.getProperty("dateFormat." + chosenDateFormatSelector);
		}
		else {
			Random rand = new Random();
			int randomisedDateFormatSelector = rand.nextInt(propertiesDateFormats.size()) + 1;
			chosenDateFormat = propertiesDateFormats.getProperty("dateFormat." + randomisedDateFormatSelector);
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(chosenDateFormat);
		String dateAsString = simpleDateFormat.format(indate);
		return dateAsString;
	}

	
}