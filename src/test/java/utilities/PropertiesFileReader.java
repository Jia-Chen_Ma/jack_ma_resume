package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileReader {
	
	public static Properties getAgedCareEmailDetails() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/agedCareEmailDetails.properties"));
		} catch (Exception e) { }
		return properties;
	}
	
	public static Properties getProperties() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/browser-config.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}

	public static Properties getmailosaurProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/mailosaur.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getmailinatorProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/mailinator.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}	
	
	public static Properties getEWSProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/ewsClient.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	
	public static Properties getMSGFileParseProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/msgFileParse.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getActiveDirectoryIntegrationProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/ADintegration.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}

	public static Properties getConnectionsProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/environmentConnectionServer.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getChosenTestEnvironmentProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/environmentSelection.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	/**
	 * 
	 * @return 
	 * @throws IOException
	 */
	public static Properties getCharacterSetProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/characterSet.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getDateFormatSetProperty() throws IOException{
		
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/dateFormatSet.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getEmailFormatSetProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/emailFormatSet.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getPhoneFormatSetProperty() throws IOException{
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/phoneFormatSet.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public Properties getMsoftSystemsAuthenticationConnectionParameters() throws IOException{
		
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/msoftSystemAuth.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	}
	
	public static Properties getChosenEnvironment() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/agedCareChosenEnvironment.properties"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return properties;
	} 
}
