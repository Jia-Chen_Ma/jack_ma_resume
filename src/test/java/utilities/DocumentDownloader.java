package utilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

/**
 * a class which takes a URL and a system directory and will download the file located at the url to the directory
 * @author jiach
 *@since 6/6/2019
 */
public class DocumentDownloader {
	
	
	/**
	 * 
	 * a method which takes a URL and a system directory and will download the file located at the url to the directory
	 * @param directory is the system directory which the file is going to be downloaded to 
	 * @param URL is the URL of the file needing to be downloaded
	 * @return a string of the filename So that we can use it with metadata and text extraction tools
	 * @throws IOException
	 */
	
	//"http://localhost:8080/sample.doc"
	//"http://localhost:8080/demo.docx"
	//"http://localhost:8080/test.pdf"
	
	public String downLoadWithJava(String directory, String URL) throws IOException {
		
		// TODO Auto-generated method stub
			String filename = java.util.UUID.randomUUID().toString();
			URL url = new URL(URL);
			filename += "."+ FilenameUtils.getExtension(url.getPath());
			File file = new File(directory +"\\"+ filename );
			System.out.println(directory + filename);
			System.out.println(url);
			file.createNewFile();

			FileUtils.copyURLToFile(url, file);
			return filename;
		}
}
