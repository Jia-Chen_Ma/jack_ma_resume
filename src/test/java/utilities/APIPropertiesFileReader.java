package utilities;

import java.io.FileInputStream;
import java.util.Properties;

public class APIPropertiesFileReader {



	public static Properties getAPIConnectivityParameters() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/APIConnectivityParameters.properties"));
		} catch (Exception e) { }
		return properties;
	}



	public static Properties getAWSProperties(){
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/AWSSetUp.properties"));
		} catch (Exception e) { }
		return properties;
	}

	public static Properties getAWSSQLRDSProperties(){
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("resources/SQLSetting.properties"));
		} catch (Exception e) { }
		return properties;
	}
}
