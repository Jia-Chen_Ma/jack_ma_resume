package windowsAuth;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;


public class AuthByRobot {

	public static void writeString(String s) throws Exception{
		Robot robot;
		robot = new Robot();

		Class clazz = KeyEvent.class;
		Field field;

		robot.delay(1000);

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);

		//	System.out.println("Expecting to print" + c);

			String otherChars = Character.toString(c);    

			switch (otherChars) {
			case " ":
				robot.keyPress(KeyEvent.VK_SPACE);
				robot.keyRelease(KeyEvent.VK_SPACE);
				continue;
			case "!":

				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_1);
				robot.keyRelease(KeyEvent.VK_1);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				/* The following ideally should work - but robot is brittle so local key board mapping is used
                  robot.keyPress(KeyEvent.VK_EXCLAMATION_MARK);
                  robot.keyRelease(KeyEvent.VK_EXCLAMATION_MARK);
				 */
				continue;
			case "\\": 
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyRelease(KeyEvent.VK_BACK_SLASH);
				continue;
			case "#":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                   robot.keyPress(KeyEvent.VK_NUMBER_SIGN);
                  robot.keyRelease(KeyEvent.VK_NUMBER_SIGN);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_3);
				robot.keyRelease(KeyEvent.VK_3);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				continue;
			case "&":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
				 * robot.keyPress(KeyEvent.VK_AMPERSAND);
                   robot.keyRelease(KeyEvent.VK_AMPERSAND);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_7);
				robot.keyRelease(KeyEvent.VK_7);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "'":
				robot.keyPress(KeyEvent.VK_QUOTE);
				robot.keyRelease(KeyEvent.VK_QUOTE);
				continue;
			case "(":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                  robot.keyPress(KeyEvent.VK_LEFT_PARENTHESIS);
                  robot.keyRelease(KeyEvent.VK_LEFT_PARENTHESIS);
				 */

				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_9);
				robot.keyRelease(KeyEvent.VK_9);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				continue;
			case ")":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                  robot.keyPress(KeyEvent.VK_RIGHT_PARENTHESIS);
                  robot.keyRelease(KeyEvent.VK_RIGHT_PARENTHESIS);
				 */

				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_0);
				robot.keyRelease(KeyEvent.VK_0);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				continue;
			case "*":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                   robot.keyPress(KeyEvent.VK_ASTERISK);
                   robot.keyRelease(KeyEvent.VK_ASTERISK);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_8);
				robot.keyRelease(KeyEvent.VK_8);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "+": 
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                   robot.keyPress(KeyEvent.VK_PLUS);
                   robot.keyRelease(KeyEvent.VK_PLUS);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_EQUALS);
				robot.keyRelease(KeyEvent.VK_EQUALS);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case ",":
				robot.keyPress(KeyEvent.VK_COMMA);
				robot.keyRelease(KeyEvent.VK_COMMA);
				continue;
			case "-":
				robot.keyPress(KeyEvent.VK_MINUS);
				robot.keyRelease(KeyEvent.VK_MINUS);
				continue;
			case ".":
				robot.keyPress(KeyEvent.VK_PERIOD);
				robot.keyRelease(KeyEvent.VK_PERIOD);
				continue;
			case "/":
				robot.keyPress(KeyEvent.VK_SLASH);
				robot.keyRelease(KeyEvent.VK_SLASH);
				continue;
			case ":":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                   robot.keyPress(KeyEvent.VK_COLON);
                   robot.keyRelease(KeyEvent.VK_COLON);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_SEMICOLON);
				robot.keyRelease(KeyEvent.VK_SEMICOLON);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case ";":
				robot.keyPress(KeyEvent.VK_SEMICOLON);
				robot.keyRelease(KeyEvent.VK_SEMICOLON);
				continue;
			case "<":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                   robot.keyPress(KeyEvent.VK_LESS);
                   robot.keyRelease(KeyEvent.VK_LESS);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_COMMA);
				robot.keyRelease(KeyEvent.VK_COMMA);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "=":
				robot.keyPress(KeyEvent.VK_EQUALS);
				robot.keyRelease(KeyEvent.VK_EQUALS);
				continue;
			case ">":
				/* The following ideally should work - but robot is brittle so local key board mapping is used
                   robot.keyPress(KeyEvent.VK_GREATER );
                   robot.keyRelease(KeyEvent.VK_GREATER );
				 */

				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_PERIOD);
				robot.keyRelease(KeyEvent.VK_PERIOD);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				continue;
			case "?":
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_SLASH);
				robot.keyRelease(KeyEvent.VK_SLASH);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "@":
				/* The following ideally should work - but robot is brittle so local key board mapping is used 
                   robot.keyPress(KeyEvent.VK_AT);
                   robot.keyRelease(KeyEvent.VK_AT);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_2);
				robot.keyRelease(KeyEvent.VK_2);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				continue;
			case "[":
				robot.keyPress(KeyEvent.VK_OPEN_BRACKET);
				robot.keyRelease(KeyEvent.VK_OPEN_BRACKET);
				continue;
			case "]":
				robot.keyPress(KeyEvent.VK_CLOSE_BRACKET);
				robot.keyRelease(KeyEvent.VK_CLOSE_BRACKET);
				continue;
			case "//":
				robot.keyPress(KeyEvent.VK_SLASH);
				robot.keyRelease(KeyEvent.VK_SLASH);
				continue;
			case "^":                  
				/* The following ideally should work - but robot is brittle so local key board mapping is used 
                   robot.keyPress(KeyEvent.VK_CIRCUMFLEX);
                   robot.keyRelease(KeyEvent.VK_CIRCUMFLEX);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_6);
				robot.keyRelease(KeyEvent.VK_6);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "_":
				/* The following ideally should work - but robot is brittle so local key board mapping is used 
                   robot.keyPress(KeyEvent.VK_UNDERSCORE);
                   robot.keyRelease(KeyEvent.VK_UNDERSCORE);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_MINUS);
				robot.keyRelease(KeyEvent.VK_MINUS);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "`":
				robot.keyPress(KeyEvent.VK_BACK_QUOTE);
				robot.keyRelease(KeyEvent.VK_BACK_QUOTE);
				continue;
			case "{":
				/* The following ideally should work - but robot is brittle so local key board mapping is used 
                   robot.keyPress(KeyEvent.VK_BRACELEFT);
                   robot.keyRelease(KeyEvent.VK_BRACELEFT);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_OPEN_BRACKET);
				robot.keyRelease(KeyEvent.VK_OPEN_BRACKET);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "|":
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_SLASH);
				robot.keyRelease(KeyEvent.VK_BACK_SLASH);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue;
			case "}":
				/* The following ideally should work - but robot is brittle so local key board mapping is used 
                   robot.keyPress(KeyEvent.VK_BRACERIGHT);
                   robot.keyRelease(KeyEvent.VK_BRACERIGHT);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_CLOSE_BRACKET);
				robot.keyRelease(KeyEvent.VK_CLOSE_BRACKET);
				robot.keyRelease(KeyEvent.VK_SHIFT);

				continue;
			case "~":
				/* The following ideally should work - but robot is brittle so local key board mapping is used 
                   robot.keyPress(KeyEvent.VK_DEAD_TILDE);
                   robot.keyRelease(KeyEvent.VK_DEAD_TILDE);
				 */
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(KeyEvent.VK_BACK_QUOTE);
				robot.keyRelease(KeyEvent.VK_BACK_QUOTE);
				robot.keyRelease(KeyEvent.VK_SHIFT);
				continue; 
			}

			String variableName = "VK_" + Character.toString(c).toUpperCase();
			field = clazz.getField( variableName );
			int keyCode = field.getInt(null);

			// System.out.println("Variablename" + );

			// if the character is a digit or if it is a letter character and lower case 


			if (Character.isDigit(c) || (Character.isLetter(c) && !Character.isUpperCase(c))){
				robot.keyPress(keyCode);
				robot.keyRelease(keyCode);
				//System.out.println(c);
				//System.out.println("digit or lowercase " + c + " --- Keycode " + keyCode);
				continue;
			}

			if (Character.isLetter(c) && Character.isUpperCase(c)) {
				robot.keyPress(KeyEvent.VK_SHIFT);
				robot.keyPress(Character.toUpperCase(c));
				robot.keyRelease(Character.toUpperCase(c));
				robot.keyRelease(KeyEvent.VK_SHIFT);
			//	System.out.println("Upper case letter " + c + " --- Keycode " + keyCode);
				continue;
			}

		}
	}
	
	public static void robotPressEnter() throws InterruptedException, AWTException {
		Robot robot;
		robot = new Robot();
		
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}
	
	public static void robotPresesTab() throws AWTException {
		Robot robot;
		robot = new Robot();

		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
	}
}



