package AWSS3UploadAndRDSConnections;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import utilities.APIPropertiesFileReader;
import utilities.PropertiesFileReader;



public class AWSEnvironmenConfig{
	static APIPropertiesFileReader propReader = new APIPropertiesFileReader();
	static Properties prop;
	final static String env = PropertiesFileReader.getChosenEnvironment().getProperty("ChosenEnvironment");

	public static void configEnvironmentFromPropertiesFile() throws IOException {
		prop = getProperties();
		if((prop.getProperty(env+"AWSProxyRequired")).equalsIgnoreCase("true")) {
			System.setProperty("http.proxyHost", prop.getProperty(env+"AWSProxyHost"));
			System.setProperty("http.proxyPort", prop.getProperty(env+"AWSProxyPort"));
			if(!(prop.getProperty(env+"AWSProxyUser")).equalsIgnoreCase("") && !(prop.getProperty(env+"AWSProxyPass")).equalsIgnoreCase("")) {
				System.setProperty("http.proxyUser", prop.getProperty(env+"AWSProxyUser"));
				System.setProperty("http.proxyPassword", prop.getProperty(env+"AWSProxyPass"));
			}
		}else {
			System.clearProperty("http.proxyHost");
			System.clearProperty("http.proxyPort");
			System.clearProperty("http.proxyUser");
			System.clearProperty("http.proxyPassword");
		}
		if(!(prop.getProperty(env+"AWSAccessKeyId")).equalsIgnoreCase("") &&!(prop.getProperty(env+"AWSSecretKey")).equalsIgnoreCase("")) {
			System.setProperty("aws.accessKeyId", prop.getProperty(env+"AWSAccessKeyId"));
			System.setProperty("aws.secretAccessKey", prop.getProperty(env+"AWSSecretKey"));
		}
	}

	private static Properties getProperties(){
		Properties prop = APIPropertiesFileReader.getAWSProperties();
		return prop;
	}

	public static String getBucketFromPropFile() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSBucket")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSBucket");
			}
			else {
				throw new Exception("No AWS Bucket is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWS Bucket is entered in the properties file",e);
		}	
	}

	public static String getConnTimeOut() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSConnectionTimeOut")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSConnectionTimeOut");
			}
			else {
				throw new Exception("No AWS Connection timeout is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWS Connection timeout is entered in the properties file",e);
		}	
	}

	public static String getSecretKey() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSecretKey")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSecretKey");
			}
			else {
				throw new Exception("No AWS secret key is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWS secret key is entered in the properties file",e);
		}	
	}

	public static String getAcessKey() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSAccessKeyId")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSAccessKeyId");
			}
			else {
				throw new Exception("No AWS access key is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWS access key is entered in the properties file",e);
		}	
	}

	public static List<List<String>> getDirectory() throws Exception {
		prop = getProperties();

		List<List<String>> listOfListOfFoldersToUpload = new ArrayList<List<String>>();
		try {
			Enumeration enumProperties = prop.propertyNames();
			System.out.println("hello");
			System.out.println(env);
			List<String> listOfFoldersToUpload = new ArrayList<String>();
			
			while(enumProperties.hasMoreElements()) {
				System.out.println(prop);
				String key = (String) enumProperties.nextElement();
				String copiableFileKeyNum = "";
				if (key.toUpperCase().contains("sourceFolderSet".toUpperCase())){
					listOfFoldersToUpload.add(prop.getProperty(key));
					copiableFileKeyNum = key.substring(20);
				}

				if (!copiableFileKeyNum.isEmpty()){
					listOfFoldersToUpload.addAll(Arrays.asList(prop.getProperty(env+"copiableFileNameSubStringSet"+copiableFileKeyNum).split(",")));
					while(!listOfFoldersToUpload.isEmpty()) {
						List<String> temp =  new ArrayList<String>();
						Iterator<String> itr = listOfFoldersToUpload.iterator();
						
						while (itr.hasNext()) {
							temp.add(itr.next());
						}
						
						listOfListOfFoldersToUpload.add(temp);
						listOfFoldersToUpload.clear();
					}
					
				}
			}

			return listOfListOfFoldersToUpload;

		}catch(Exception e) {
			throw new Exception("No AWS directory is entered in the properties file",e);
		}	
	}

	public static String getRegion() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSRegion")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSRegion");
			}
			else {
				throw new Exception("No AWS Region is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWS Region is entered in the properties file",e);
		}	
	}
	
	
}

