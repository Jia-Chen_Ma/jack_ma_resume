package AWSS3UploadAndRDSConnections;

import java.sql.*;


public class AWSRdsConnection {
	Connection conn;
    String query;
    Statement st;
	public AWSRdsConnection() {
			String myDriver = AWSSQLRdsEnvironmenConfig.getSQLDriver();
	      
	     
	      
	      try {
		    Class.forName(myDriver);

		conn = DriverManager.getConnection(AWSSQLRdsEnvironmenConfig.getSQLURL(), AWSSQLRdsEnvironmenConfig.getSQLUser(), AWSSQLRdsEnvironmenConfig.getSQLPass());

			st = conn.createStatement();
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	      
	}
	
	public void  setQueryToReturnAllResultsFromACEMON() {
		query= "select * from "+ AWSSQLRdsEnvironmenConfig.getSQLTable();
		try {
			viewTableOfTestCaseResults();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setQueryToInsert() {
		query = "INSERT into "+ AWSSQLRdsEnvironmenConfig.getSQLTable()
	      		+ AWSSQLRdsEnvironmenConfig.getAWSSQLInsertTableQuery()
	      		+ AWSSQLRdsEnvironmenConfig.getAWSSQLInsertTableValue(); 
	      		executeUpdateDelete();
	}
	
	public void setQueryToDelete() {
		query = "DELETE FROM "+ AWSSQLRdsEnvironmenConfig.getSQLTable()
				+ AWSSQLRdsEnvironmenConfig.getAWSSQLDeleteTableFiltration();
		executeUpdateDelete();
	}
	
	public void setQueryToUpdate() {
		query = "UPDATE `"+ AWSSQLRdsEnvironmenConfig.getSQLTable()+"`"
				+AWSSQLRdsEnvironmenConfig.getAWSSQLUpdateTableSetValue()
				+AWSSQLRdsEnvironmenConfig.getAWSSQLUpdateTableFiltration();
		executeUpdateDelete();
	}
	
	
	private void executeUpdateDelete() {
		try {
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void viewTableOfTestCaseResults() throws ClassNotFoundException, SQLException {
		
	      
	      
	      // execute the query, and get a java resultset
	      ResultSet resultSet = st.executeQuery(query);
	      ResultSetMetaData rsmd = resultSet.getMetaData();
	      int columnsNumber = rsmd.getColumnCount();
	      while (resultSet.next()) {
	          for (int i = 1; i <= columnsNumber; i++) {
	              if (i > 1) System.out.print(",  ");
	              String columnValue = resultSet.getString(i);
	              System.out.print(columnValue + " " + rsmd.getColumnName(i));
	          }
	          System.out.println("");
	      }
	      
	    

	}
}
