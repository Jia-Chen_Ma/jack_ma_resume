package AWSS3UploadAndRDSConnections;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import utilities.APIPropertiesFileReader;
import utilities.PropertiesFileReader;



public class AWSSQLRdsEnvironmenConfig{
	static APIPropertiesFileReader propReader = new APIPropertiesFileReader();
	static Properties prop;
	final static String env = PropertiesFileReader.getChosenEnvironment().getProperty("ChosenEnvironment");

	

	private static Properties getProperties(){
		Properties prop = APIPropertiesFileReader.getAWSSQLRDSProperties();
		return prop;
	}

	public static String getSQLCustomerName() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLcustomerName")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLcustomerName");
			}
			else {
				throw new Exception("No AWSSQLcustomerName is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWSSQLcustomerName is entered in the properties file",e);
		}	
	}
	
	public static String getSQLCustomerMission() throws Exception {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLcustomerMission")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLcustomerMission");
			}
			else {
				throw new Exception("No AWSSQLcustomerMission is entered in the properties file");
			}
		}catch(Exception e) {
			throw new Exception("No AWSSQLcustomerMission is entered in the properties file",e);
		}	
	}
	
	public static String getSQLURL() {
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLDataBaseURL")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLDataBaseURL");
			}
			else {
				throw new Exception("No AWSSQLDataBaseURL is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLDataBaseURL is entered in the properties file");
			return"";
		}	
	}
	
	public static String getSQLDriver(){
		prop = getProperties();
		System.out.println(env+"AWSSQLDriver");
		try {
			if(!(prop.getProperty(env+"AWSSQLDriver")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLDriver");
			}
			else {
				throw new Exception("No AWSSQLDriver is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLDriver is entered in the properties file");
			return"";
		}	
	}
	
	public static String getSQLUser(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLUserName")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLUserName");
			}
			else {
				throw new Exception("No AWSSQLUserName is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLUserName is entered in the properties file");
			return"";
		}	
	}
	
	public static String getSQLPass(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLPassword")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLPassword");
			}
			else {
				throw new Exception("No AWSSQLPassword is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLPassword is entered in the properties file");
			return"";
		}	
	}
	
	public static String getSQLTable(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLTable")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLTable");
			}
			else {
				throw new Exception("No AWSSQLTable is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLTable is entered in the properties file");
			return"";
		}	
	}
	
	public static String getAWSSQLInsertTableQuery(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLInsertTableQuery")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLInsertTableQuery");
			}
			else {
				throw new Exception("No AWSSQLInsertTableQuery is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLInsertTableQuery is entered in the properties file");
			return"";
		}	
	}
	
	public static String getAWSSQLInsertTableValue(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLInsertTableValue")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLInsertTableValue");
			}
			else {
				throw new Exception("No AWSSQLInsertTableValue is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLInsertTableValue is entered in the properties file");
			return"";
		}	
	}
	
	public static String getAWSSQLDeleteTableFiltration(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLDeleteTableFiltration")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLDeleteTableFiltration");
			}
			else {
				throw new Exception("No AWSSQLDeleteTableFiltration is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLDeleteTableFiltration is entered in the properties file");
			return"";
		}	
	}
	
	public static String getAWSSQLUpdateTableFiltration(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLUpdateTableFiltration")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLUpdateTableFiltration");
			}
			else {
				throw new Exception("No AWSSQLUpdateTableFiltration is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLUpdateTableFiltration is entered in the properties file");
			return"";
		}	
	}
	
	public static String getAWSSQLUpdateTableSetValue(){
		prop = getProperties();
		try {
			if(!(prop.getProperty(env+"AWSSQLUpdateTableSetValue")).equalsIgnoreCase("")) {
				return prop.getProperty(env+"AWSSQLUpdateTableSetValue");
			}
			else {
				throw new Exception("No AWSSQLUpdateTableSetValue is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No AWSSQLUpdateTableSetValue is entered in the properties file");
			return"";
		}	
	}
	
	
}

