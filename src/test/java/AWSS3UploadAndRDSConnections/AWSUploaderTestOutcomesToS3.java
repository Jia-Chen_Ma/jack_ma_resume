package AWSS3UploadAndRDSConnections;

import java.io.File;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

import utilities.FormattedDataGenerator;
import software.amazon.awssdk.auth.credentials.SystemPropertyCredentialsProvider;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.http.apache.ProxyConfiguration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

public class AWSUploaderTestOutcomesToS3 {
	static List <PutObjectRequest> objRequestList = new ArrayList<PutObjectRequest>();
	static Iterator<File> selectedFilesForUploadWithDirectory;
	static S3Client s3Client;
	static List<List<String>> listOfSetsOfFileDirectives;
	//typically a set may have
	//1.the source directory that we wish to upload
	//2.the description of we want to upload
	private static ArrayList<String> itemsToUpload;

	public static void uploadDirectoryToS3(String businessArea) throws Exception {
		AWSEnvironmenConfig.configEnvironmentFromPropertiesFile();
		listOfSetsOfFileDirectives = AWSEnvironmenConfig.getDirectory();
		
		SdkHttpClient httpclient = ApacheHttpClient.builder()
				.proxyConfiguration(ProxyConfiguration.builder()
						.useSystemPropertyValues(true)
						.build())
				.connectionTimeout(Duration.ofSeconds(Long.parseLong(AWSEnvironmenConfig.getConnTimeOut())))
				.build();
		s3Client = S3Client.builder()
				.httpClient(httpclient)
				.region(Region.of(AWSEnvironmenConfig.getRegion()))
				.credentialsProvider(SystemPropertyCredentialsProvider.create())
				.build();

		System.out.println(listOfSetsOfFileDirectives);
		scanAndUpload(businessArea);
	}

	private static void scanAndUpload(String businessArea) throws Exception {
		SimpleDateFormat dateFormattor = new SimpleDateFormat("yyyy.MM.dd'_'hh.mm.ss'_'a'_'zzz");
		String date = dateFormattor.format(new Date());
		while(!listOfSetsOfFileDirectives.isEmpty()) {
			List<File> files = enumerateAndFindReleventFiles(listOfSetsOfFileDirectives.remove(0));
			System.out.println("files at scan and upload: "+files);
			selectedFilesForUploadWithDirectory = files.iterator();
			//example [target\Reports\ExtentReport.html, target\Reports\ExtentReport.html, target\Reports\report.js, target\Reports\results.csv, target\Reports\tempEmailContent.txt]
			upLoadFilesInS3Directories(businessArea,date);
			System.out.println("files after upload: "+files);
		}
	}

	private static List<File> enumerateAndFindReleventFiles(List<String> singleSetOfFileDirectives) {
		//singleSetOfFileDirectives example:
		//list = target, ExtentReport, results*
		System.out.println("hello");
		String targetFolder = singleSetOfFileDirectives.remove(0);
		List<File> listOfReleventFiles = new ArrayList<File>();
		System.out.println("hello" +singleSetOfFileDirectives);
		System.out.println("contain or not: "+singleSetOfFileDirectives.contains("*"));
		while (!singleSetOfFileDirectives.isEmpty()) {
			if(!singleSetOfFileDirectives.get(0).contains("*")) {
				//if the the sourceFolderSets does not have a * only the surface level will be scaned for relevent items and uploaded

				if(!targetFolder.contains("*")) {
						Iterator<File> ListOfFilesInTargetDirectory = FileUtils.iterateFiles(new File(targetFolder), null, false);
						String filesToCheck = singleSetOfFileDirectives.remove(0);
						while(ListOfFilesInTargetDirectory.hasNext()) {
							String fileDIR = ListOfFilesInTargetDirectory.next().toString();
							fileDIR = fileDIR.replace(targetFolder+"\\", "");
							System.out.println(fileDIR);
							if(fileDIR.toUpperCase().contains(filesToCheck.toUpperCase())) {
								System.out.println(fileDIR+" horray found it");
								listOfReleventFiles.add(new File(targetFolder+"\\"+fileDIR));
							}
					}
				}else {
					//if sourceFolderSets have a * all sub directory will be uploaded and scaned
					targetFolder = targetFolder.replace("*", "");
					(FileUtils.iterateFiles(new File(targetFolder), null, true)).forEachRemaining(listOfReleventFiles::add);
					break;
				}
				
				//if copiableFileNameSubStringSet has a * then this will trigger and scan the directory level and upload all files in that directory, no sub directorys will be uploaded
			}else {
				singleSetOfFileDirectives.remove(0);
				(FileUtils.iterateFiles(new File(targetFolder), null, false)).forEachRemaining(listOfReleventFiles::add);
				break;
			}
		}


		return listOfReleventFiles;

	}

	// wht doe the long line do
	private static void upLoadFilesInS3Directories(String businessArea,String date) throws Exception {
		HashMap<String,String> metadata = new HashMap<String,String>();
		itemsToUpload = new ArrayList<String>();

		while(selectedFilesForUploadWithDirectory.hasNext()) {
			File file=selectedFilesForUploadWithDirectory.next();
			metadata.put("fileLocation", file.getPath().replace("\\", "/"));
			PutObjectRequest obj = PutObjectRequest.builder()
					.bucket(AWSEnvironmenConfig.getBucketFromPropFile())
					.key(FormattedDataGenerator.getDateFormatted(new Date(), 7)+"/"+businessArea+"/"+date+"/"+file.getPath().replace("\\", "/"))
					//.key(FormattedDataGenerator.getDateFormatted(new Date(), 7)+"/"+businessArea+"/"+directory+dateFormattor.format(new Date())+"/"+file.getPath().substring(directory.length()+1).replace("\\", "/")) 
					.metadata(metadata)
					.build();
			objRequestList.add(obj);
			System.out.println(file.getPath().replace("\\", "/"));
			System.out.println(obj.key());
			itemsToUpload.add(FormattedDataGenerator.getDateFormatted(new Date(), 7)+"/"+businessArea+"/"+date+"/"+file.getPath().replace("\\", "/"));
		}

		for(PutObjectRequest obj:objRequestList) {
			s3Client.putObject(obj, Paths.get(obj.metadata().get("fileLocation")));
		}

	}

	public static String getReportURL() {
		String reportURL = null;
		for(String s : itemsToUpload) {
			if(s.contains("ExtentReport.html")) {
				reportURL = s;
				break;
			}
		}
		return reportURL;
	}

}
