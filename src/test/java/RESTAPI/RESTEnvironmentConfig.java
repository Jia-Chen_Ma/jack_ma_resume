package RESTAPI;

import java.util.Properties;

import utilities.APIPropertiesFileReader;
import utilities.PropertiesFileReader;

public class RESTEnvironmentConfig {
	static Properties prop = APIPropertiesFileReader.getAPIConnectivityParameters();
	
	
	public static String getHost() {
		try {
			if(!(prop.getProperty("host")).equalsIgnoreCase("")) {
				return prop.getProperty("host");
			}
			else {
				throw new Exception("No host is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No host is entered in the properties file");return "";
		}	
	}
	
	public static String getPort() {
		try {
			if(!(prop.getProperty("port")).equalsIgnoreCase("")) {
				return prop.getProperty("port");
			}
			else {
				throw new Exception("No port is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No port is entered in the properties file");return "";
		}	
	}
	
	public static String getProtocol() {
		try {
			if(!(prop.getProperty("protocol")).equalsIgnoreCase("")) {
				return prop.getProperty("protocol");
			}
			else {
				throw new Exception("No protocol is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No protocol is entered in the properties file");return "";
		}	
	}
	
	public static String getWorkstation() {
		try {
			if(!(prop.getProperty("workstation")).equalsIgnoreCase("")) {
				return prop.getProperty("workstation");
			}
			else {
				throw new Exception("No workstation is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No workstation is entered in the properties file");return "";
		}	
	}
	
	public static String getDomain() {
		try {
			if(!(prop.getProperty("domain")).equalsIgnoreCase("")) {
				return prop.getProperty("domain");
			}
			else {
				throw new Exception("No domain is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No domain is entered in the properties file");return "";
			
		}	
	}
	
	public static String getUser() {
		try {
			if(!(prop.getProperty("user")).equalsIgnoreCase("")) {
				return prop.getProperty("user");
			}
			else {
				throw new Exception("No user is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No user is entered in the properties file");
			return "";
		}
			
	}
	
	public static String getPass() {
		try {
			if(!(prop.getProperty("pass")).equalsIgnoreCase("")) {
				return prop.getProperty("pass");
			}
			else {
				throw new Exception("No pass is entered in the properties file");
			}
		}catch(Exception e) {
			System.out.println("No pass is entered in the properties file");return "";
		}	
	}
	
}
